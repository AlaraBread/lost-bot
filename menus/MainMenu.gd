extends Control

func _ready():
	$StartButton.grab_focus()
	MusicManager.play(6)
	if(GameSaver.error != ""):
		info(GameSaver.error)
		GameSaver.error = ""

func _on_StartButton_pressed():
	if(GameSaver.get("level")==0):
		Loader.goto_scene("res://cutscenes/Cutscene0.tscn")
		return
	Loader.goto_scene("res://levels/Level%d.tscn"%[clamp(GameSaver.get("level"), 1, 15)])

func _on_Reset_pressed():
	GameSaver.reset()

func _on_LevelEditor_pressed():
	update_level_code()
	Loader.goto_scene("res://editor/Editor.tscn")

func _on_PlayCustom_pressed():
	update_level_code()
	Loader.goto_scene("res://editor/EditorLevel.tscn")

func update_level_code():
	if($CustomLevel/LevelCodeEdit.text != ""):
		GameSaver.level_code = $CustomLevel/LevelCodeEdit.text

func _on_MainMenu_pressed():
	$CustomLevelButton.grab_focus()
	$CustomLevel.visible = false

func _on_CustomLevelButton_pressed():
	$CustomLevel/MainMenu.grab_focus()
	$CustomLevel.visible = true

func _on_Info_focus_entered():
	$StartButton.grab_focus()
	$Info.visible = false

func info(info:String):
	$InfoTimer.start()
	$Info.visible = true
	$Info/Label.text = info

func _on_InfoTimer_timeout():
	_on_Info_focus_entered()

func _on_PasteLevel_pressed():
	$CustomLevel/LevelCodeEdit.text = OS.get_clipboard()

func _on_Credits_pressed():
	Loader.goto_scene("res://menus/Credits.tscn")

func _on_Rebind_pressed():
	$RebindMenu.visible = true
	$RebindMenu/MainMenu.grab_focus()

func _on_MainMenu_rebind_pressed():
	$RebindMenu.visible = false
	$Rebind.grab_focus()

