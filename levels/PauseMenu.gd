extends Control

func _input(event):
	if(not event is InputEventKey):
		return
	if(Input.is_action_just_pressed("pause")):
		_on_PauseButton_toggled(not get_tree().paused)

func _on_Unpause_pressed():
	_on_PauseButton_toggled(false)

func _on_PauseButton_toggled(button_pressed):
	if(get_node("../WinMenu").visible):
		return
	get_tree().paused = button_pressed
	get_node("../PauseButton").pressed = button_pressed
	get_node("../../Camera").set_can_move(get_node("../../Camera").can_move)
	visible = get_tree().paused
	if(visible):
		$Unpause.grab_focus()
