extends Control
class_name Level

export(int) var music:int = 0
export(float) var bpm = 145
export(int) var level_number:int = 1
export(bool) var right_to_cutscene = false
export(String) var cutscene:String = ""
var pitstop:Vector2 = Vector2()
var robot

var tile_info = [
		{type="wall", sound=6},#0 tree
		{type="floor"},#1
		{type="pitstop", count=3},#2
		{type="floor"},#3
		{type="wall", sound=7},#4
		{type="wall", sound=8},#5
		{type="null"},#6
		{type="breakable", breaks_into=8, sound=3},#7
		{type="breakable", breaks_into=18, sound=4},#8
		{type="death"},#9
		{type="end"},#10
		{type="wall", sound=9},#11 rock
		{type="pitstop", count=4},#12
		{type="floor"},#13
		{type="pitstop", count=3},#14
		{type="pitstop", count=4},#15
		{type="end"},#16
		{type="floor"},#17
		{type="floor"},#18
		{type="wall", sound=9},#19
		{type="wall", sound=9},#20
		{type="wall", sound=9},#21
		{type="wall", sound=9},#22
		{type="floor"},#23 spooky grass
		{type="wall", sound=9}, #24 skull
		{type="wall", sound=6}, #25 spooky tree
		{type="wall", sound=6}, #26 pumpkin
		{type="pitstop", count=3}, #27 world 3 red tent
		{type="pitstop", count=4}, #28 world 3 blue tent
		{type="floor"}, #29 bug
		{type="death"}, #30 world 3 spike
		{type="end"}, #31
		{type="breakable", breaks_into=33, sound=4},#32 spooky cracked wall 1
		{type="breakable", breaks_into=23, sound=3} #33 spooky cracked wall 2
	]

export(String, MULTILINE) var start_message = ""
export(String, MULTILINE) var tent_message = ""
var stuck_message = "I'm stuck.\nPress 'R' to go back"

func pick_weighed(distribution:Array):
	var sum = 0
	for d in distribution:
		sum += d
	var r = rand_range(0, sum)
	for i in range(len(distribution)):
		r -= distribution[i]
		if(r <= 0):
			return i
	return len(distribution)-1

const stuck_messages = [
	"Hey! I'm stuck ‘ere! (In New Yorkian)\nPress 'R' to restart.",
	"Hey bozo, I don’t know if you’ve noticed, but we’re kinda going nowhere...\nPress 'R' to restart.",
	"My mother always said I wouldn't make it far...\nPress 'R' to restart.",
	"I should’ve just been a doctor.\nPress 'R' to restart.",
	"Good news! The GPS is showing me an ETA of… 5 secs… 5 sextillion years if it’s up to you.\nPress 'R' to restart.",
	"You know, hearing this music repeat is kinda getting annoying wouldn’t you agree? How about you do something about it?      Press 'R' to restart.",
	"Ya know, I don’t encourage cheating, but just skip this one… please...\nPress 'R' to restart.",
	"The fitness gram pacer test is a multistage aerobic capacity test...\nPress 'R' to restart.",
	"I’m stuck.\nPress 'R' to restart.",
	"I’m stuck.\nPresione 'R' para comenzar de nuevo."
]
const stuck_dist = [2, 2, 2, 2, 2, 2, 2, 2, 8, 1]
func get_stuck_message()->String:
	return stuck_messages[pick_weighed(stuck_dist)]

func _setup()->bool:
	return true

var objects = []
var conveyors = []
func _ready():
	var success = _setup()
	if(not success):
		return
	$FrameTimer.wait_time = 60/bpm
	$ResetTimer.wait_time = 60/bpm
	MusicManager.play(music)
	
	if(level_number > 0):
		$CanvasLayer/LevelLabel.text = " %d-%d "%[((level_number-1)/5)+1, ((level_number-1)%5)+1]
	else:
		$CanvasLayer/LevelLabel.queue_free()
	
	if(GameSaver.get("level") < level_number):
		GameSaver.set("level", level_number)
	
	#randomize tiles
	
	#var floors = []
	#var walls = []
	#for i in range(len(tile_info)):
		#if(tile_info[i]["type"]=="floor"):
			#floors.append(i)
		#elif(tile_info[i]["type"]=="wall"):
			#walls.append(i)
	#eed(69) # nice
	#for pos in $TileMap.get_used_cells():
		#var type = tile_info[$TileMap.get_cellv(pos)]["type"]
		#if(type == "wall"):
			#$TileMap.set_cellv(pos, walls[pick_weighed(wall_distribution)])
		#elif(type == "floor"):
			#$TileMap.set_cellv(pos, floors[pick_weighed(floor_distribution)])
	for placer in $Objects.get_children():
		var object = placer.object.instance()
		object.position = $ObjectMap.world_to_map(placer.rect_position)
		object.revert_pos = object.position
		if(object.is_in_group("robot")):
			robot = object
			pitstop = object.position
		if(object.is_in_group("conveyor")):
			object.direction = placer.direction
			conveyors.append(object)
		else:
			objects.append(object)
		placer.queue_free()
		set_object_pos(object, Vector2(-10000,-10000))
		object.level = self
	$CanvasLayer/Selectors.show_selectors(tile_info[$TileMap.get_cellv(pitstop)]["count"])
	dialogue(start_message)

var dialogue_visible = false
func dialogue(message:String):
	if(message == ""):
		if(not dialogue_visible):
			return
		$CanvasLayer/Dialogue/AnimationPlayer.play("close")
		dialogue_visible = false
	else:
		if(dialogue_visible):
			return
		$CanvasLayer/Dialogue/Text.text = message
		$CanvasLayer/Dialogue/AnimationPlayer.play("open")
		dialogue_visible = true

func _input(_event):
	if(Input.is_action_just_pressed("reset")):
		reset()

func reset_later():
	if($ResetTimer.time_left != 0):
		return
	dead = true
	$ResetTimer.start()

func _on_ResetTimer_timeout():
	reset()

func reset():
	if($CanvasLayer/WinMenu.visible):
		return
	stuck = false
	$Camera.set_can_move(true)
	dead = false
	$FrameTimer.stop()
	dialogue("")
	$CanvasLayer/Selectors.show_selectors(tile_info[$TileMap.get_cellv(pitstop)]["count"])
	for tile in $ObjectMap.get_used_cells():
		$ObjectMap.set_cellv(tile, -1)
	for o in objects:
		o.position = o.revert_pos
		set_object_pos(o, Vector2(-10000,-10000))
	for r in revert_tiles:
		$TileMap.set_cellv(r[0], r[1])

var robot_dirs: = []
var cur_robot_dir = 0
func start_game():
	$Camera.set_can_move(false)
	if(dialogue_visible):
		$CanvasLayer/Dialogue/AnimationPlayer.play("close")
		dialogue_visible = false
	cur_robot_dir = 0
	robot_dirs = $CanvasLayer/Selectors.get_dirs()
	time_since_move = 0
	$FrameTimer.start()

func _on_FrameTimer_timeout():
	frame()
	animation_frame()

func animation_frame():
	for o in objects:
		o.frame = posmod(o.frame+1, o.animation_length)

var revert_tiles = []
#pushes the passed object in a direction specified by old_pos
#returns whether or not it successfully moved a space
func push(o, old_pos:Vector2)->bool:
	var tile = $TileMap.get_cellv(o.position)
	if(tile == -1 or tile_info[tile]["type"]=="wall"):
		if(tile_info[tile].has("sound")):
			play_sfx(tile_info[tile]["sound"], o.position)
		return false
	if(tile_info[tile]["type"]=="breakable"):
		if(o.is_in_group("robot")):
			$TileMap.set_cellv(o.position, tile_info[tile]["breaks_into"])
			revert_tiles.insert(0, [o.position, tile])
			play_sfx(tile_info[tile]["sound"], o.position)
			return tile_info[tile_info[tile]["breaks_into"]]["type"] == "floor"
		else:
			return false
	#check if on conveyor
	var on_conveyor = false
	for c in conveyors:
		if(c.position == old_pos):
			on_conveyor = true
			break
	if(not on_conveyor):
		for c in conveyors:
			if(c.position == o.position and c.direction == old_pos-o.position):
				return false
	for col in objects:
		if(col != o and col.position == o.position):
			if(col.pushable):
				var col_old_pos = col.position
				col.position += o.position-old_pos
				if(push(col, col_old_pos)):
					set_object_pos(col, col_old_pos)
					return true
				else:
					col.position = col_old_pos
					return false
			else:
				return false
	return true

func play_sfx(id, pos):
	var sfx = preload("res://levels/SFXPlayer.tscn").instance()
	sfx.position = pos*64
	add_child(sfx)
	sfx.play_sfx(id)

var stuck = false
func object_frame(o):
	var old_pos = o.position
	o.frame()
	#check for collisions with other objects
	if(not push(o, old_pos)):
		o.position = old_pos
	set_object_pos(o, old_pos)
	var tile = $TileMap.get_cellv(o.position)
	if(o.is_in_group("robot")):
		if(tile_info[tile]["type"] == "death"):
			dead = true
			reset_later()
		#check for pitstop
		if(not dead and old_pos != o.position and tile_info[tile]["type"] == "pitstop"):
			pitstop(o)
		#check for portal
		if(not dead and tile_info[tile]["type"] == "end"):
			win()
		#shake
		if(old_pos == o.position):
			$Camera.shake()
			time_since_move += 1
		else:
			play_sfx(walk_sfx, o.position)
			time_since_move = 0
			if(stuck):
				dialogue("")
			stuck = false
		if(time_since_move > $CanvasLayer/Selectors.count):
			if(not stuck):
				dialogue(get_stuck_message())
			stuck = true
	else:
		if(old_pos != o.position):
			play_sfx(5, o.position)
	o.moved = false

export(int) var walk_sfx:int = 0
var dead = false
var time_since_move = 0
func frame():
	for o in objects:
		if(o != robot):
			object_frame(o)
	object_frame(robot)
	if(dead):
		return
	var played_conveyor = false
	for o in conveyors:
		for col in objects:
			if(col != o and not col.moved and col.position == o.position):
				var col_old_pos = col.position
				col.position += o.direction
				col.moved = true
				if(push(col, col_old_pos)):
					set_object_pos(col, col_old_pos)
					if(not played_conveyor):
						play_sfx(2, o.position)
				else:
					col.position = col_old_pos
				if(col.is_in_group("robot")):
					#check for pitstop
					var tile = $TileMap.get_cellv(col.position)
					if(col.revert_pos != col.position and tile_info[tile]["type"] == "pitstop"):
						pitstop(col)
					#check for portal
					if(tile_info[tile]["type"] == "end"):
						win()
					if(tile_info[tile]["type"] == "death"):
						reset_later()
					#shake
					if(col_old_pos == col.position):
						$Camera.shake()

func pitstop(rob):
	if(tent_message != ""):
		dialogue(tent_message)
		tent_message = ""
	$Camera.set_can_move(true)
	var tile = $TileMap.get_cellv(rob.position)
	$CanvasLayer/Selectors.show_selectors(tile_info[tile]["count"])
	$FrameTimer.stop()
	pitstop = rob.position
	for r in objects:
		r.revert_pos = r.position
	revert_tiles = []

func set_object_pos(o, old_pos:Vector2):
	if(o.is_in_group("conveyor")):
		$ConveyorMap.set_cellv(old_pos, -1, o.flip_x, o.flip_y) # set the object's current position to empty
		$ConveyorMap.set_cellv(o.position, o.sprite_ind+o.frame, o.flip_x, o.flip_y) # sets the objects current position to the correct frame
	else:
		$ObjectMap.set_cellv(old_pos, -1, o.flip_x, o.flip_y) # set the object's current position to empty
		$ObjectMap.set_cellv(o.position, o.sprite_ind+o.frame, o.flip_x, o.flip_y) # sets the objects current position to the correct frame
		if(o.is_in_group("robot")):
			$Camera.position = o.position*64+Vector2(32,32)

func next_robot_dir()->Vector2:
	var out = robot_dirs[cur_robot_dir]
	$CanvasLayer/Selectors.highlight(cur_robot_dir)
	cur_robot_dir = posmod(cur_robot_dir+1, len(robot_dirs))
	return out

func win():
	if(right_to_cutscene and cutscene != ""):
		Loader.goto_scene(cutscene)
		return
	var l = GameSaver.get("level")
	if(level_number >= l):
		GameSaver.set("level", level_number+1)
	$FrameTimer.stop()
	$CanvasLayer/WinMenu.visible = true
	if(is_instance_valid($CanvasLayer/WinMenu/NextLevel)):
		$CanvasLayer/WinMenu/NextLevel.grab_focus()
	else:
		$CanvasLayer/WinMenu/RestartLevel.grab_focus()

func _on_NextLevel_pressed():
	get_tree().paused = false
	if(cutscene != ""):
		Loader.goto_scene(cutscene)
		return
	Loader.goto_scene("res://levels/Level%d.tscn"%[level_number+1])

func _on_MainMenu_pressed():
	get_tree().paused = false
	Loader.goto_scene("res://menus/MainMenu.tscn")

func _on_RestartLevel_pressed():
	get_tree().paused = false
	if(level_number > 0):
		Loader.goto_scene("res://levels/Level%d.tscn"%[level_number])
	else:
		Loader.goto_scene("res://editor/EditorLevel.tscn")
