extends Node

func hex_to_int(hex:String)->int:
	var number:int = 0
	for c in range(len(hex)):
		match hex[len(hex)-c-1]:
			"0":
				number += 0*(pow(16, c))
			"1":
				number += 1*(pow(16, c))
			"2":
				number += 2*(pow(16, c))
			"3":
				number += 3*(pow(16, c))
			"4":
				number += 4*(pow(16, c))
			"5":
				number += 5*(pow(16, c))
			"6":
				number += 6*(pow(16, c))
			"7":
				number += 7*(pow(16, c))
			"8":
				number += 8*(pow(16, c))
			"9":
				number += 9*(pow(16, c))
			"a":
				number += 10*(pow(16, c))
			"b":
				number += 11*(pow(16, c))
			"c":
				number += 12*(pow(16, c))
			"d":
				number += 13*(pow(16, c))
			"e":
				number += 14*(pow(16, c))
			"f":
				number += 15*(pow(16, c))
	return number

func compress(string:String)->String:
	if(len(string)<1):
		return string
	# convert the string into a 2d array that holds each character and how many times it is repeated
	var array:Array = []
	var c:int = 0
	while(c < len(string)):
		var count:int = 0
		for i in range(c, len(string)):
			if(string[i] != string[c]):
				break
			else:
				count += 1
		array.append([string[c], count])
		c += count
	#convert the array into a compressed string
	var compressed:String = ""
	for i in array:
		if(i[1] >= 4):
			compressed += "(%c%02x"%[i[0], i[1]]
		else:
			for _j in range(i[1]):
				compressed += i[0]
	return compressed

func decompress(string:String)->String:
	var decompressed:String = ""
	var c:int = 0
	while(c < len(string)):
		if(string[c] == '(' and c < len(string)-3):
			var repeat = string[c+1]
			var count = hex_to_int(string.substr(c+2,2))
			c += 3
			for _i in range(count):
				decompressed += repeat
		else:
			decompressed += string[c]
		c += 1
	return decompressed
