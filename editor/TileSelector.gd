extends TextureRect

signal selected(s)
export(int) var tile = 0
func _on_TileSelector_gui_input(event):
	if(event is InputEventMouseButton and event.button_index == BUTTON_LEFT and event.pressed):
		select()

func select():
	emit_signal("selected", self)
	$ColorRect.visible = true

func unselect():
	$ColorRect.visible = false
