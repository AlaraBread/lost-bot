extends Level

func data_to_tile(data:int):
	return data-1

func data_to_object(data:int):
	if(data==0):
		return -1
	return data-1

const OBJECTS = [
	preload("res://levels/objects/Robot.tscn"),
	preload("res://levels/objects/Box.tscn"),
	preload("res://levels/objects/Box2.tscn"),
	preload("res://levels/objects/Box3.tscn"),
	preload("res://levels/objects/Robot2.tscn"),
	]

const CONVEYORS = [
	preload("res://levels/objects/Conveyor.tscn"),
	preload("res://levels/objects/Conveyor2.tscn"),
	preload("res://levels/objects/Conveyor3.tscn"),
]

func _setup()->bool:
	$Objects/Robot.free()
	$CanvasLayer/WinMenu/NextLevel.queue_free()
	$CanvasLayer/PauseMenu/NextLevel.queue_free()
	
	var code = StringCompressor.decompress(GameSaver.level_code)
	var array:PoolByteArray = Marshalls.base64_to_raw(code)
	if(len(array) != 129):
		invalid_code()
		return false
	var cur_ind = 0
	var world = array[cur_ind]
	match world:
		0:
			music = 0
			$CanvasLayer/StaticPaper.visible = true
			$CanvasLayer/StaticPaperVignette.visible = false
			$CanvasLayer/StaticPaperVignette2.visible = false
			walk_sfx = 0
			$Camera/Light2D.visible = false
		1:
			music = 1
			$CanvasLayer/StaticPaper.visible = false
			$CanvasLayer/StaticPaperVignette.visible = true
			$CanvasLayer/StaticPaperVignette2.visible = false
			walk_sfx = 1
			$Camera/Light2D.visible = false
		2:
			music = 3
			$CanvasLayer/StaticPaper.visible = false
			$CanvasLayer/StaticPaperVignette.visible = false
			$CanvasLayer/StaticPaperVignette2.visible = true
			walk_sfx = 0
			$Camera/Light2D.visible = true
		3:
			music = 4
			$CanvasLayer/StaticPaper.visible = false
			$CanvasLayer/StaticPaperVignette.visible = false
			$CanvasLayer/StaticPaperVignette2.visible = true
			walk_sfx = 0
			$Camera/Light2D.visible = true
		4:
			music = 5
			$CanvasLayer/StaticPaper.visible = false
			$CanvasLayer/StaticPaperVignette.visible = false
			$CanvasLayer/StaticPaperVignette2.visible = true
			walk_sfx = 0
			$Camera/Light2D.visible = true
	cur_ind += 1
	for x in range(8):
		for y in range(8):
			var data = array[cur_ind]
			cur_ind += 1
			#object
			var o_data = (data&0b11110000)>>4
			if(o_data > 0 and o_data <= len(OBJECTS)):
				var o = OBJECTS[o_data-1].instance()
				objects.append(o)
				o.position = Vector2(x, y)
				o.revert_pos = o.position
				if(o.is_in_group("robot")):
					robot = o
					pitstop = o.position
				set_object_pos(o, Vector2(-10000,-10000))
				o.level = self
			#conveyor
			var c_data = data&0b00001111
			if(c_data&0b1100 > 0):
				var c = CONVEYORS[((c_data&0b1100)>>2)-1].instance()
				if(c_data&0b0010==0):
					#vertical
					if(c_data&0b0001==0):
						c.set_direction(Vector2(0, -1))
					else:
						c.set_direction(Vector2(0, 1))
				else:
					#horizontal
					if(c_data&0b0001==0):
						c.set_direction(Vector2(1, 0))
					else:
						c.set_direction(Vector2(-1, 0))
				c.position = Vector2(x, y)
				set_object_pos(c, Vector2(-10000, -10000))
				conveyors.append(c)
			data = array[cur_ind]
			cur_ind += 1
			#tile
			$TileMap.set_cell(x, y, data_to_tile(data))
	#make sure its a valid level
	if(robot == null):
		invalid_level("Missing Player")
		return false
	if(tile_info[$TileMap.get_cellv(robot.position)]["type"] != "pitstop"):
		$TileMap.set_cellv(robot.position, 2)
	return true

func invalid_code():
	get_tree().paused = false
	GameSaver.error = "Invalid Level Code"
	Loader.goto_scene("res://menus/MainMenu.tscn")

func invalid_level(e:String=""):
	get_tree().paused = false
	GameSaver.error = "Invalid Level: " + e
	Loader.goto_scene("res://editor/Editor.tscn")

func _on_CopyLevel_pressed():
	OS.set_clipboard(GameSaver.level_code)

func _on_EditLevel_pressed():
	get_tree().paused = false
	Loader.goto_scene("res://editor/Editor.tscn")
