extends Control

func _ready():
	for c in $TileSelector/ScrollTile/TileSelectors.get_children():
		c.connect("selected", self, "tile_selected")
	for c in $TileSelector/ConveyorSelectors.get_children():
		c.connect("selected", self, "tile_selected")
	for c in $TileSelector/ObjectSelectors.get_children():
		c.connect("selected", self, "tile_selected")
	
	if(not load_from_string(GameSaver.level_code)):
		_on_MusicSelector_item_selected(0)
	
	_on_Tiles_pressed()
	
	if(GameSaver.error != ""):
		info(GameSaver.error)
		GameSaver.error = ""

func tile_selected(selector):
	unselect_all()
	selected_tile = selector.tile
	flip_x = selector.flip_h
	flip_y = selector.flip_v

var selected_tile = 0
var flip_x = false
var flip_y = false

var clicked = false
var right_clicked = false
func _unhandled_input(event):
	if(event is InputEventMouseButton):
		if(event.button_index == BUTTON_LEFT):
			clicked = event.pressed
		elif(event.button_index == BUTTON_RIGHT):
			right_clicked = event.pressed
		handle_mouse(event.position)
	if((clicked or right_clicked) and event is InputEventMouseMotion):
		var pos = event.position
		handle_mouse(pos)

export(Vector2) var max_size = Vector2(8,8)
func handle_mouse(pos:Vector2):
	pos = (pos-tilemap.position)/64
	pos.x = clamp(pos.x, 0, max_size.x-1)
	pos.y = clamp(pos.y, 0, max_size.y-1)
	if(clicked):
		tilemap.set_cellv(pos, selected_tile, flip_x, flip_y)
	if(right_clicked):
		tilemap.set_cellv(pos, -1, flip_x, flip_y)

func unselect_all():
	for c in $TileSelector/ScrollTile/TileSelectors.get_children():
		c.unselect()
	for c in $TileSelector/ConveyorSelectors.get_children():
		c.unselect()
	for c in $TileSelector/ObjectSelectors.get_children():
		c.unselect()

func _on_LayerSelector_item_selected(index):
	match index:
		0:
			_on_Tiles_pressed()
		1:
			_on_Conveyors_pressed()
		2:
			_on_Objects_pressed()

onready var tilemap = $TileMap
func _on_Objects_pressed():
	tilemap = $ObjectMap
	$TileSelector/ObjectSelectors.visible = true
	$TileSelector/ScrollTile.visible = false
	$TileSelector/ConveyorSelectors.visible = false
	unselect_all()
	$TileSelector/ObjectSelectors.get_child(0).select()

func _on_Tiles_pressed():
	tilemap = $TileMap
	$TileSelector/ObjectSelectors.visible = false
	$TileSelector/ScrollTile.visible = true
	$TileSelector/ConveyorSelectors.visible = false
	unselect_all()
	$TileSelector/ScrollTile/TileSelectors.get_child(0).select()

func _on_Conveyors_pressed():
	tilemap = $ConveyorMap
	$TileSelector/ObjectSelectors.visible = false
	$TileSelector/ScrollTile.visible = false
	$TileSelector/ConveyorSelectors.visible = true
	unselect_all()
	$TileSelector/ConveyorSelectors.get_child(0).select()



func tile_to_data(tile:int)->int:
	return tile+1

func data_to_tile(data:int)->int:
	return data-1

func bool_to_int(b:bool)->int:
	if(b):
		return 1
	else:
		return 0

func conveyor_to_data(tile:int, x_flip:bool, y_flip:bool)->int:
	if(tile<0):
		return 0
	var data:int = 0
	match int(tile/2):
		0:
			#world 1
			data += 0b0100
		1:
			#world 2
			data += 0b1000
		2:
			#world 3
			data += 0b1100
	if(tile%2==0):
		#vertical
		data += bool_to_int(y_flip)
	else:
		#horizontal
		data += bool_to_int(x_flip)
	data += (tile%2)<<1
	return data

func data_to_conveyor(data:int)->int:
	if(data&0b1100 == 0):
		return -1
	match data&0b1100:
		0b1100:
			#world 3
			return 4+((data&0b0010)>>1)
		0b1000:
			#world 2
			return 2+((data&0b0010)>>1)
		0b0100:
			#world 1
			return 0+((data&0b0010)>>1)
		0b0000:
			#empty
			return -1
	return -1

func data_to_conveyor_flip(data:int)->bool:
	return data&0b0001==1

func object_to_data(tile:int)->int:
	if(tile < 0):
		return 0
	if(tile == 5):
		return 3
	if(tile == 6):
		return 4
	if(tile==8):
		return 5
	return tile+1

func data_to_object(data:int)->int:
	if(data == 0):
		return -1
	if(data == 3):
		return 5
	if(data == 4):
		return 6
	if(data==5):
		return 8
	return data-1

func serialize()->String:
	var array:PoolByteArray = []
	array.append($TileSelector/MusicSelector.selected)
	for x in range(8):
		for y in range(8):
			var data = 0
			data += object_to_data($ObjectMap.get_cell(x, y))<<4
			data += conveyor_to_data($ConveyorMap.get_cell(x, y),
					$ConveyorMap.is_cell_x_flipped(x, y),
					$ConveyorMap.is_cell_y_flipped(x, y))
			array.append(data)
			array.append(tile_to_data($TileMap.get_cell(x, y)))
	var string = Marshalls.raw_to_base64(array)
	return StringCompressor.compress(string)

func load_from_string(string:String)->bool:
	string = StringCompressor.decompress(string)
	var array:PoolByteArray = Marshalls.base64_to_raw(string)
	if(len(array) != 129):
		invalid_code()
		return false
	var cur_ind = 0
	$TileSelector/MusicSelector.select(array[cur_ind])
	_on_MusicSelector_item_selected(array[cur_ind])
	cur_ind += 1
	for x in range(8):
		for y in range(8):
			var data = array[cur_ind]
			cur_ind += 1
			var o_data = (data&0b11110000)>>4
			$ObjectMap.set_cell(x, y, data_to_object(o_data))
			var c_data = (data&0b00001111)
			var tile = data_to_conveyor(c_data)
			if(tile == 0):
				#vertical
				$ConveyorMap.set_cell(x, y, tile, false, data_to_conveyor_flip(c_data))
			else:
				#horizontal
				$ConveyorMap.set_cell(x, y, tile, data_to_conveyor_flip(c_data), false)
			data = array[cur_ind]
			cur_ind += 1
			$TileMap.set_cell(x, y, data_to_tile(data))
	return true

func invalid_code():
	GameSaver.level_code = ""

func _on_Test_pressed():
	GameSaver.level_code = serialize()
	Loader.goto_scene("res://editor/EditorLevel.tscn")

func _on_MainMenu_pressed():
	GameSaver.level_code = serialize()
	Loader.goto_scene("res://menus/MainMenu.tscn")


func _on_Info_focus_entered():
	$Info.visible = false

func info(info:String):
	$InfoTimer.start()
	$Info.visible = true
	$Info/Label.text = info

func _on_InfoTimer_timeout():
	_on_Info_focus_entered()

func _on_MusicSelector_item_selected(index):
	if(index >= 2):
		index += 1
	MusicManager.play(index)
